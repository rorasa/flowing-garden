const leafA = {
  alpha: {
    list: [
      {
        value: 0.25,
        time: 0
      },
      {
        value: 0.5,
        time: 1
      }
    ],
    isStepped: false
  },
  scale: {
    list: [
      {
        value: 0.25,
        time: 0
      },
      {
        value: 0.5,
        time: 1
      }
    ],
    minimumScaleMultiplier: 2
  },
  color: {
    list: [
      {
        value: "fb1010",
        time: 0
      },
      {
        value: "f5b830",
        time: 1
      }
    ],
    isStepped: false
  },
  speed: {
    start: 80,
    end: 50,
    minimumSpeedMultiplier: 1.5
  },
  acceleration: {
    x: 0,
    y: 0
  },
  maxSpeed: 0,
  startRotation: {
    min: 0,
    max: 360
  },
  rotationSpeed: {
    min: 0,
    max: 60
  },
  lifetime: {
    min: 3,
    max: 5
  },
  frequency: 0.003,
  spawnChance: 1,
  emitterLifetime: -1,
  maxParticles: 400,
  pos: {
    x: 0,
    y: 0
  },
  addAtBack: false,
  spawnType: "point",
};
const leafB = {
  alpha: {
    list: [
      {
        value: 0.25,
        time: 0
      },
      {
        value: 0.5,
        time: 1
      }
    ],
    isStepped: false
  },
  scale: {
    list: [
      {
        value: 0.25,
        time: 0
      },
      {
        value: 0.5,
        time: 1
      }
    ],
    minimumScaleMultiplier: 2
  },
  color: {
    list: [
      {
        value: "00DBF1",
        time: 0
      },
      {
        value: "54F999",
        time: 1
      }
    ],
    isStepped: false
  },
  speed: {
    start: 80,
    end: 50,
    minimumSpeedMultiplier: 1.5
  },
  acceleration: {
    x: 0,
    y: 0
  },
  maxSpeed: 0,
  startRotation: {
    min: 0,
    max: 360
  },
  rotationSpeed: {
    min: 0,
    max: 60
  },
  lifetime: {
    min: 3,
    max: 5
  },
  frequency: 0.003,
  spawnChance: 1,
  emitterLifetime: -1,
  maxParticles: 400,
  pos: {
    x: 0,
    y: 0
  },
  addAtBack: false,
  spawnType: "point",
};
const leafC = {
  alpha: {
    list: [
      {
        value: 0.25,
        time: 0
      },
      {
        value: 0.5,
        time: 1
      }
    ],
    isStepped: false
  },
  scale: {
    list: [
      {
        value: 0.25,
        time: 0
      },
      {
        value: 0.5,
        time: 1
      }
    ],
    minimumScaleMultiplier: 2
  },
  color: {
    list: [
      {
        value: "B9DA0B",
        time: 0
      },
      {
        value: "F1DF00",
        time: 1
      }
    ],
    isStepped: false
  },
  speed: {
    start: 80,
    end: 50,
    minimumSpeedMultiplier: 1.5
  },
  acceleration: {
    x: 0,
    y: 0
  },
  maxSpeed: 0,
  startRotation: {
    min: 0,
    max: 360
  },
  rotationSpeed: {
    min: 0,
    max: 60
  },
  lifetime: {
    min: 3,
    max: 5
  },
  frequency: 0.003,
  spawnChance: 1,
  emitterLifetime: -1,
  maxParticles: 400,
  pos: {
    x: 0,
    y: 0
  },
  addAtBack: false,
  spawnType: "point",
};
const leafD = {
  alpha: {
    list: [
      {
        value: 0.25,
        time: 0
      },
      {
        value: 0.5,
        time: 1
      }
    ],
    isStepped: false
  },
  scale: {
    list: [
      {
        value: 0.25,
        time: 0
      },
      {
        value: 0.5,
        time: 1
      }
    ],
    minimumScaleMultiplier: 2
  },
  color: {
    list: [
      {
        value: "F1EDE4",
        time: 0
      },
      {
        value: "F9F8DF",
        time: 1
      }
    ],
    isStepped: false
  },
  speed: {
    start: 80,
    end: 50,
    minimumSpeedMultiplier: 1.5
  },
  acceleration: {
    x: 0,
    y: 0
  },
  maxSpeed: 0,
  startRotation: {
    min: 0,
    max: 360
  },
  rotationSpeed: {
    min: 0,
    max: 60
  },
  lifetime: {
    min: 3,
    max: 5
  },
  frequency: 0.003,
  spawnChance: 1,
  emitterLifetime: -1,
  maxParticles: 400,
  pos: {
    x: 0,
    y: 0
  },
  addAtBack: false,
  spawnType: "point",
};

const butterflyA = {
  alpha: {
    list: [
      {
        value: 0.8,
        time: 0
      },
      {
        value: 0.6,
        time: 1
      }
    ],
    isStepped: false
  },
  scale: {
    list: [
      {
        value: 0.25,
        time: 0
      },
      {
        value: 0.5,
        time: 1
      }
    ],
    minimumScaleMultiplier: 2
  },
  color: {
    list: [
      {
        value: "f5b830",
        time: 0
      },
      {
        value: "fb1010",
        time: 0.25
      },
      {
        value: "e40374",
        time: 0.5
      },
      {
        value: "b109f1",
        time: 0.75
      },
      {
        value: "685ef9",
        time: 1
      }
    ],
    isStepped: false
  },
  speed: {
    start: 80,
    end: 50,
    minimumSpeedMultiplier: 1.5
  },
  acceleration: {
    x: 0,
    y: 0
  },
  maxSpeed: 0,
  startRotation: {
    min: 0,
    max: 360
  },
  rotationSpeed: {
    min: 0,
    max: 60
  },
  lifetime: {
    min: 3,
    max: 5
  },
  frequency: 0.003,
  spawnChance: 1,
  emitterLifetime: -1,
  maxParticles: 400,
  pos: {
    x: 0,
    y: 0
  },
  addAtBack: false,
  spawnType: "circle",
  spawnCircle: {
    x: 0,
    y: 0,
    r: 20
  }
}

const PARTICLES = {
  leafA: leafA,
  leafB: leafB,
  leafC: leafC,
  leafD: leafD,
  butterflyA: butterflyA
};
