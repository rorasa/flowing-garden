const express = require('express');
const path = require('path');
const http = require('http');
const io = require('socket.io')(http);
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

let season = "spring";
let photomode = true;

app.listen(3000, ()=>{
  console.log("Listening on port 3000");
});
// app.get('/', (req, res)=>{
//   res.send("Flowing Garden Weather Control Server");
// });

app.use('/', express.static(path.resolve(__dirname,'client')));

app.get('/status', (req,res)=>{
  res.send({
    season: season,
    photomode: photomode
  });
})

app.post('/seasonchange', (req, res)=>{
  const request = req.body;

  console.log("Change season to "+request.season);

  season = request.season;
  io.sockets.emit('season_changed');
  res.sendStatus(200);
})

app.post('/photomode', (req,res)=>{
  const request = req.body;

  console.log((request.photomode)?"Toggle photomode on":"Toggle photomode off");

  photomode = request.photomode;
  io.sockets.emit('mode_changed');
  res.sendStatus(200);
})
