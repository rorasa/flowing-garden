const electron = require('electron')

const {app, BrowserWindow} = require('electron')

function createWindow(){
  win = new BrowserWindow({
    width: 1200,
    height: 800,
    fullscreen: true,
  });

  win.loadFile('index.html');

  // win.webContents.openDevTools();
}

app.on('ready', createWindow);
