import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import SpringButton from './spring.jpg';
import SummerButton from './summer.jpg';
import AutumnButton from './autumn.jpg';
import WinterButton from './winter.jpg';
import PhotoButton from './photo.jpg';

class SeasonControl extends Component {
  constructor(props){
    super(props);

    this.state = {
      currentseason: "spring"
    };

    this.handleSeasonChange = this.handleSeasonChange.bind(this);
  }

  handleSeasonChange(season){

    fetch('/seasonchange', {
      method: "POST",
      headers: {
        "Content-type":"application/json"
      },
      body: JSON.stringify({
        season: season
      })
    }).then(res=>{
      if(!res.status===200){
        return;
      }
      fetch('/status').then(res=>res.json())
      .then(data=>{
        this.setState({
          currentseason: data.season
        });
      });
    });
  }

  render(){

    return(
      <div>
        <h2>Season</h2>

        <div>
          <input className={(this.state.currentseason==="spring")?"seasonbutton active":"seasonbutton"}
            type="image" id="spring" alt="spring"
            onClick={()=>{this.handleSeasonChange("spring")}}
            src={SpringButton}/>
          <input className={(this.state.currentseason==="summer")?"seasonbutton active":"seasonbutton"}
            type="image" id="summer" alt="summer"
            onClick={()=>{this.handleSeasonChange("summer")}}
            src={SummerButton}/>
          <input className={(this.state.currentseason==="autumn")?"seasonbutton active":"seasonbutton"}
            type="image" id="autumn" alt="autumn"
            onClick={()=>{this.handleSeasonChange("autumn")}}
            src={AutumnButton}/>
          <input className={(this.state.currentseason==="winter")?"seasonbutton active":"seasonbutton"}
            type="image" id="winter" alt="winter"
            onClick={()=>{this.handleSeasonChange("winter")}}
            src={WinterButton}/>
        </div>
      </div>
    );
  }
}

class PhotoMode extends Component{
  constructor(props){
    super(props);

    this.state = {
      photomode: false
    }

    this.handleToggle = this.handleToggle.bind(this);
  }

  handleToggle(){
    fetch('/photomode', {
      method: "POST",
      headers: {
        "Content-type":"application/json"
      },
      body: JSON.stringify({
        photomode: !this.state.photomode
      })
    }).then(res=>{
      if(!res.status===200){
        return;
      }
      fetch('/status').then(res=>res.json())
      .then(data=>{
        this.setState({
          photomode: data.photomode
        });
      });
    });
  }

  render(){
    return(
      <div>
        <h2>Photo friendly</h2>
        <div>
          <input className={(this.state.photomode)?"photobutton active":"photobutton"}
            type="image" id="photo" alt="photo"
            onClick={this.handleToggle}
            src={PhotoButton}/>
        </div>
      </div>
    );
  }
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <header>
          <h1>Weather control</h1>

          <SeasonControl/>

          <PhotoMode/>
        </header>
      </div>
    );
  }
}

export default App;
